#!/bin/bash -e

echo "================================================================"
echo "  About to delete all the source trees"
echo "================================================================"

sleep 5

./src-rm.sh

echo "================================================================"
echo "  Cloning and patching sources"
echo "================================================================"

git clone \
  https://git.morello-project.org/morello/morello-linux-headers.git

git clone \
  --branch morello/next \
  https://git.morello-project.org/morello/morello-sdk.git
    
git clone \
  https://git.morello-project.org/morello/morello-busybox.git
cd morello-busybox; patch -p1 < ../patches/morello-busybox.patch; cd ..

echo "================================================================"
echo "  Done"
echo "================================================================"

cd port
./src-get.sh
