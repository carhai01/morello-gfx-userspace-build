#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/vars.sh"

cp -par usr/include/* "$PCPREFIX/include"