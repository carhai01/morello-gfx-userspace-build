### destination for purecap chroot install
export PCPREFIX="/opt/pc2"
### llvm toolchain prefix already installed on system
export PREFIX="/morello/llvm"
### arch string
export A="aarch64-linux-musl_purecap"
### -march
export M="morello"
### ABI
export ABI="purecap"
### dynamic linker
export DL="-Wl,--dynamic-linker=/lib/ld-musl-morello.so.1"

