#!/bin/bash -e

PC=$( realpath ${BASH_SOURCE[0]} )
PC=$( dirname "$PC" )
PC=$( realpath "$PC" )
. "$PC/conf.sh"
export PC

echo "====== install sdk libc etc. ======"
mkdir -p "$PCPREFIX/bin" "$PCPREFIX/lib" "$PCPREFIX/include"
cp -par /morello/musl/lib/* "$PCPREFIX/lib/"
cp -par /morello/musl/include/* "$PCPREFIX/include/"
pushd "$PCPREFIX/lib/"
ln -sf libc.so ld-musl-morello.so.1
popd
echo "====== install sdk libc etc. DONE ======"

echo "====== linux headers ======"
cd morello-linux-headers
../build-morello-linux-headers.sh
cd ..
echo "====== linux headers DONE ======"

echo "====== morello busybox ======"
cd morello-busybox
../build-morello-busybox.sh
cd ..
echo "====== morello busybox DONE ======"

cd port
./build.sh
cd ..
echo "====== graphics stack userspace port DONE ======"
