#!/bin/bash -e

. "$PC/port/vars.sh"

m4 \
-D PREFIX="$PREFIX" \
-D PCPREFIX="$PCPREFIX" \
-D A="$A" \
-D M="$M" \
-D ABI="$ABI" \
-D DL="$DL" \
meson-cross.txt.in > meson-cross.txt
