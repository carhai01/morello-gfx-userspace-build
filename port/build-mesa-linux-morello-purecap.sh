#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

rm -rf build
meson setup --cross-file ../meson-cross.txt build \
  --prefix=/ \
  -D b_ndebug=true \
  -D buildtype=plain \
  --wrap-mode=nofallback \
  -D sysconfdir=/etc \
  -D platforms=wayland \
  -D gallium-drivers=panfrost,kmsro,swrast \
  -D vulkan-drivers= \
  -D gallium-opencl=disabled \
  -D opencl-spirv=false \
  -D glx=disabled \
  -D egl=enabled \
  -D gbm=enabled \
  -D gles1=enabled \
  -D gles2=enabled \
  -D libunwind=disabled \
  -D lmsensors=disabled \
  -D valgrind=disabled \
  -D osmesa=true \
  -D tools=[]
ninja -C build
DESTDIR="$PCPREFIX" ninja -C build install
