#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/conf.sh"

export LDFLAGS="-static"

rm -rf build
mkdir build
cd build
../configure \
  --prefix=/
make -j5
cp -pa bash "$PCPREFIX/bin/"
