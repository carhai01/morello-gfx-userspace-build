#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/port/vars.sh"

autoreconf -v -i

rm -rf build
mkdir build
cd build
../configure \
  --prefix=/ \
  --host="$A" \
  --without-python
make -j5
DESTDIR="$PCPREFIX" make install
