#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/port/vars.sh"

cd expat

autoreconf -v -i

rm -rf build
mkdir build
cd build
../configure \
  --prefix=/ \
  --host="$A" \
  --without-docbook
make -j5
DESTDIR="$PCPREFIX" make install
