#!/bin/bash -e

rm -rf \
  bash \
  binutils-gdb \
  drm-linux-morello-purecap \
  zlib \
  libffi-morello-linux-purecap \
  libexpat \
  xz \
  libxml2 \
  wayland \
  wayland-protocols \
  mesa-linux-morello-purecap \
  kmscube
