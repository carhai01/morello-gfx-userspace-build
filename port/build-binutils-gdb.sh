#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/conf.sh"

export LDFLAGS="-static"

rm -rf build
mkdir build
cd build
../configure \
  --disable-threading \
  --disable-werror \
  --host=aarch64-linux-gnu \
  --target=aarch64-linux-gnu
make -j5 all-gdb
strip gdb/gdb
install gdb/gdb "$PCPREFIX/bin/gdb"
