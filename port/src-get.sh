#!/bin/bash -e

echo "================================================================"
echo "  About to delete all the source trees"
echo "================================================================"

sleep 5

./src-rm.sh

echo "================================================================"
echo "  Installing needed build packages not already there"
echo "================================================================"

apt install \
  libtool \
  autopoint \
  python3-mako

echo "================================================================"
echo "  Installing needed updated autoconf tool for bnuilds"
echo "================================================================"

dpkg -i autoconf_2.71-3_all.deb

echo "================================================================"
echo "  Cloning and patching sources"
echo "================================================================"

git clone --branch \
  bash-5.2 \
  https://git.savannah.gnu.org/git/bash.git
git clone --branch \
  users/ARM/morello-binutils-gdb-master \
  https://git.morello-project.org/morello/gnu-toolchain/binutils-gdb.git
git clone --branch \
  morello-drm-purecap \
  https://git.morello-project.org/carhai01/drm-linux-morello-purecap.git
git clone --branch \
  v1.3 \
  https://github.com/madler/zlib.git
git clone --branch \
  morello-libffi-purecap \
  https://git.morello-project.org/carhai01/libffi-morello-linux-purecap.git
git clone --branch \
  R_2_5_0 \
  https://github.com/libexpat/libexpat.git
git clone --branch \
  v5.4.4 \
  https://github.com/xz-mirror/xz.git
git clone --branch \
  v2.12.1 \
  https://github.com/GNOME/libxml2.git
git clone --branch \
  1.21.0 \
  https://gitlab.freedesktop.org/wayland/wayland.git
git clone --branch \
  1.32 \
  https://gitlab.freedesktop.org/wayland/wayland-protocols.git
git clone --branch \
  morello-mesa-purecap \
  https://git.morello-project.org/carhai01/mesa-linux-morello-purecap.git
git clone \
  https://gitlab.freedesktop.org/mesa/kmscube.git

echo "================================================================"
echo "  Done"
echo "================================================================"
