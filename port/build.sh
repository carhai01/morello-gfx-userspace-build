#!/bin/bash -e

PC=$( realpath ${BASH_SOURCE[0]} )
PC=$( dirname "$PC" )
PC=$( realpath "$PC/../" )
. "$PC/conf.sh"
export PC

echo "================================================================"
echo "  Generate meson cross file"
echo "================================================================"
./meson-cross-gen.sh

for I in \
  bash \
  binutils-gdb \
  drm-linux-morello-purecap \
  zlib \
  libffi-morello-linux-purecap \
  libexpat \
  xz \
  libxml2 \
  wayland \
  wayland-protocols \
  mesa-linux-morello-purecap \
  kmscube \
  ; do
    echo " "
    echo " "
    echo "================================================================="
    echo " "
    echo "   $I"
    echo " "
    echo "================================================================="
    echo " "
    echo " "
    cd $I
    "../build-$I.sh"
    cd ..
done

echo "================================================================"
echo "  Done"
echo "================================================================"
