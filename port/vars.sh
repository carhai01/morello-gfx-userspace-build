. "$PC/vars.sh"

export AR="$PREFIX/bin/llvm-ar"
export LD="$PREFIX/bin/ld.lld"
export STRIP="$PREFIX/bin/llvm-strip"
export PKG_CONFIG_PATH="$PCPREFIX/lib/pkgconfig"
