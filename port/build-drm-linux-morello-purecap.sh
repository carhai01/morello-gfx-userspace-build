#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

rm -rf build
meson setup --cross-file ../meson-cross.txt build \
  --prefix=/ \
  -Dinstall-test-programs=true
ninja -C build
DESTDIR="$PCPREFIX" ninja -C build install
