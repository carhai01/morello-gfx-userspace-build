#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/port/vars.sh"

# fix unsupported relocation and symbol not found errors
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

autoreconf -v -i

rm -rf build
mkdir build
cd build
../configure \
  --prefix=/ \
  --host="$A" \
  --enable-sandbox=no
make -j5
DESTDIR="$PCPREFIX" make install
