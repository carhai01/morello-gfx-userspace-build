### toplevel purecap port directory
. "$PC/conf.sh"

L="/usr/local/lib:/usr/lib:/lib/aarch64-linux-gnu:$PCPREFIX/lib"
CF="-g --sysroot=$PCPREFIX -XCClinker --target=$A -march=$M -mabi=$ABI"
LF="-fuse-ld=lld $DL --target=$A -march=$M -mabi=$ABI"

export CC="$PREFIX/bin/clang"
export CXX="$PREFIX/bin/clang++"
export LD_LIBRARY_PATH="$L"
export PATH="$PREFIX/bin:$PATH:$PCPREFIX/bin"
export CFLAGS="$CF"
export CXXFLAGS="$CF"
export LDFLAGS="$LF"

