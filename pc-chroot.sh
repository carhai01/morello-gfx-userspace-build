#!/bin/bash
R="/opt/pc2"
CMD="/bin/sh"
MOUNTLIST=(\
/proc \
/sys \
/run \
/dev \
/dev/mqueue \
/dev/hugepages \
/dev/shm \
/dev/pts \
)

echo "==== Mounting virtual FS's ===="
for ((I=0; I<${#MOUNTLIST[@]}; I++)) do
  M=${MOUNTLIST[$I]}
  mkdir -p ${R}${M}
  mount --bind ${M} ${R}${M}
done
echo "==== Entering chroot with ${CMD} ===="
chroot ${R} ${CMD}
echo "==== Unmounting virtual FS's ===="
for ((I=${#MOUNTLIST[@]}-1; I>=0; I--)) do
  M=${MOUNTLIST[$I]}
  umount -f ${R}${M}
done
echo "==== Done ===="
