#!/bin/bash -e

if test -z "$PC"; then echo "Please set PC var to 'pc' dir path"; exit 1; fi

. "$PC/vars.sh"

export CFLAGS="$CFLAGS -I$PCPREFIX/include"

rm -rf ./build
mkdir build

cp ../morello-sdk/morello/projects/config/morello_busybox_config .config
make BUILD_DIR=./build clean
make BUILD_DIR=./build oldconfig
make BUILD_DIR=./build V=1 \
  CONFIG_STATIC=n \
  CONFIG_PREFIX="$PCPREFIX" \
  CONFIG_SYSROOT="$PCPREFIX" \
  CONFIG_EXTRA_CFLAGS="$CFLAGS" \
  CONFIG_EXTRA_LDFLAGS="$LDFLAGS" \
  -j5 install
