Build a choortable purecap ABI filesystem tree up to kmscube graphics stack
(won't actually work without kernel PCABI changes - they are in git master).
Run this from the Debian image you boot into on a morello system.

---

# Quick start guide

To get sources before any build:

```
./src-get.sh
```

To build and install (default is /opt/pc2):

```
./build.sh
```

To enter chroot and run things:

```
./pc-chroot.sh
```

To clean your build:

```
./clean.sh
```

To remove all the downloaded and patches sources:

```
./src-rm.sh
```

To delete everything installed and start from scratch (default install):

```
rm -rf /opt/pc2
```

To modify any configuration like install location etc:

```
vi conf.sh
```

---

# Status

Once you have userspace up and running and can drop into the chroot, run
`kmscube` (the hello world of of drm/accelerated graphics) as the first port of
call that fails. You can run some other tests like `proptest`, `modetest` and
`vbltest`. Tthese will work, but `kmscube` will crash early on while it
initializes. The problem is musl etc. have not properly implemented support
for thread-local variables (done with `__thread` and not
`pthread_getspecific()` / `pthread_setspecific())`. The `current` address
in `_eglGetCurrentThread()` will be NULL (it should not be) and thus
everythinfg falls over.

Relevant backtrace:

```c
Program received signal SIGSEGV, Segmentation fault
Capability tag fault while accessing address 0x0000000000000000.
_eglGetCurrentThread () at ../src/egl/main/eglcurrent.c:62
62      ../src/egl/main/eglcurrent.c: No such file or directory.
(gdb) bt
#0 _eglGetCurrentThread () at ../src/egl/main/eglcurrent.c:62
#1 0x0000fffff7e297c8 in _eglInternalError (errCode=12288,
   msg=0xfffff7df9319 [rR,0xfffff7df9319-0xfffff7df9328]
0xfffff7df9319 "eglQueryString")
   at ../src/egl/main/eglcurrent.c:97
   #2 0x0000fffff7e29404 in _eglError (errCode=12288,
      msg=0xfffff7df9319 [rR,0xfffff7df9319-0xfffff7df9328]
0xfffff7df9319 "eglQueryString")
   at ../src/egl/main/eglcurrent.c:165
   #3 0x0000fffff7e142f4 in eglQueryString (dpy=0x0 0x0, name=12373)
      at ../src/egl/main/eglapi.c:780
      #4 0x0000aaaaaac1b884 in init_egl (
         egl=0xaaaaaac4e260 [rwRW,0xaaaaaac4e260-0xaaaaaac4e470]
0xaaaaaac4e260,
   gbm=0xaaaaaac4e210 [rwRW,0xaaaaaac4e210-0xaaaaaac4e260]
0xaaaaaac4e210, samples=0)
   at ../common.c:337
   #5 0x0000aaaaaac1d40c in init_cube_smooth (
      gbm=0xaaaaaac4e210 [rwRW,0xaaaaaac4e210-0xaaaaaac4e260]
0xaaaaaac4e210 , samples=0)
   at ../cube-smooth.c:225
   #6 0x0000aaaaaac2844c in main (argc=1, argv=0xfffffffff830
[rwxRWE,0x0-0x1000000000000] 0xfffffffff830)
   at ../kmscube.c:239
```

Relevant code section:

```c
static __THREAD_INITIAL_EXEC _EGLThreadInfo _egl_TLS = {
   .inited = false,
};

static void
_eglInitThreadInfo(_EGLThreadInfo *t)
{
   t->LastError = EGL_SUCCESS;
   /* default, per EGL spec */
   t->CurrentAPI = EGL_OPENGL_ES_API;
}

/**
 * Return the calling thread's thread info.
 * If the calling thread never calls this function before, or if its thread
 * info was destroyed, reinitialize it. This function never returns NULL.
 */
_EGLThreadInfo *
_eglGetCurrentThread(void)
{
   _EGLThreadInfo *current = &_egl_TLS;

   if (unlikely(!current->inited)) { // Line 62 - crash here
      memset(current, 0, sizeof(current[0]));
      _eglInitThreadInfo(current);
      current->inited = true;
   }
   return current;
}
```

This would be the place to start (fix musl) as there needs to be a complete
working stack to drive testing ofr any ABI work. Continue filling in
everything missing or wrong from here. There are still plenty of ioctls in
DRM that transport pointers/capabilities in __u64's that need fixing. Some of
this work might be needed in libdrm (`drm-linux-morello-purecap` has the
necessary changes). Also some more Mesa work may be needed (some initial work
done in `mesa-linux-morello-purecap`). Also `libffi-morello-linux-purecap`
has relevant patches from CHERIBSD just needed for morello.

---

# Design

This port setup is split into 2 sections. A basic setup that copies in relevant
musl libc binaries etc. from the morello Debian host image into the chroot
and then a busybox build to run in the chroot. Then there is the `port`
sub-directory with everything higher up built on top that is necessary to
build and run everything.

On the kernel side, the DRM patches have started following a design pattern.
Any relevant DRM ioctl's have had a "compat" entry and exit point added. These
are the `if (in_compat64_syscall())` handlers. These act as
"protocol encode/decoders" to handle compat processes (which on moreallo are
aarch64) or native purecap processes. Relevant ioctl structs need changing
to use `__kernel_intptr_t` instead of `__u64` in relevant fields when these
are used to transport userspace pointers. In some cases these are stored for
future use (e.g. vblank events) that are then returned to userspace as valid
capabilities for purecap when the process read()s these events from the DRM
device. This requires more work to make sure these are transported correctly.

The relevant kernel code that has been touched is in `drivers/gpu/drm` and
`include/uapi/drm`.
