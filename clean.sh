#!/bin/sh

rm -rf \
build \
build-clang \
build-compiler-rt \
build-libcxx \
build-libcxxabi \
build-libunwind

rm -rf my-llvm
rm -rf morello-busybox/build

cd port
./clean.sh
